class Game {
  getWinner(p, c) {
    // this.player = p;
    // this.com = c;

    if (p === c) {
      return "Draw";
    } else if (
      (p === "gunting" && c === "kertas") ||
      (p === "kertas" && c === "batu") ||
      (p === "batu" && c === "gunting")
    ) {
      return "Player 1 Win";
    } else {
      return "Com Win";
    }
  }
}

function getComputerChoice() {
  const rps = ["gunting", "batu", "kertas"];
  const rand = Math.floor(Math.random() * 3);
  return rps[rand];
}

const playGame = new Game();

const userChoice = document.querySelectorAll(".playerChoose");
const revealWinner = document.getElementById("result");
const boxHasil = document.getElementById("boxHasil");
const reset = document.getElementById("refreshButton");

// const disableFunction() {

// }

userChoice.forEach((anjay) =>
  anjay.addEventListener("click", (e) => {
    const playerChoice = e.target.id;
    const comChoice = getComputerChoice();
    winner = playGame.getWinner(playerChoice, comChoice);
    console.log(`Player memilih ${playerChoice}`);
    console.log(`Com memilih ${comChoice}`);
    console.log(winner);

    const revealPlayerChoice = document.getElementById(playerChoice);
    const revealComChoice = document.getElementById(`gambar-${comChoice}`);

    revealWinner.innerText = `${winner}`;
    boxHasil.classList.add("boxHasil");
    revealPlayerChoice.classList.add("pilihan");
    revealComChoice.classList.add("pilihan");

    reset.addEventListener("click", function () {
      boxHasil.classList.remove("boxHasil");
      revealPlayerChoice.classList.remove("pilihan");
      revealComChoice.classList.remove("pilihan");
      revealWinner.innerText = `VS`;
    });
  })
);
