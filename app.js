const express = require("express");
const app = express();
const port = 1234;
const bodyParser = require("body-parser");
// const jsonParser = bodyParser.json();
const users = require("./users.json");

// uploading previous challenge files to server with express (view engine: ejs)
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get("/homepage", (req, res) => {
  res.render("chapter3/index");
});

app.get("/game", (req, res) => {
  res.render("chapter4/index");
});

const cek = JSON.parse(JSON.stringify(users));

const authLogin = (req, res, next) => {
  let { email, password } = req.body;

  const user = cek.data.find(
    (item) => item.email === email && item.password === password
  );
  if (!user) {
    return res.json({
      message: "Wrong email or password",
    });
  }
  next();
};

app.post("/login", authLogin, (req, res) => {
  const { email, password } = req.body;

  res.json({
    message: "Login Successful",
    data: {
      email,
      password,
    },
  });
});

app.listen(port, () => {
  console.log(`app is listening at ${port}`);
});
